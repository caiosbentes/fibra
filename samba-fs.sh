#!/bin/bash
#
# sambdadc.sh
#
# Autor     : Caio Bentes <caio.bentes@solustecnologia.com.br>
# Este Script instala um servidor de arquivos samba 4.8.0 em um SO Debian
# Este servidor está configurado como menbro de domínio
#  -------------------------------------------------------------
#   
#

#Atualiza a lista de pacotes e atualiza pacotes
apt-get update && apt-get upgrade -y
#Instalo dependencias
apt-get install -y vim acl attr autoconf bind9utils bison build-essential \
debhelper dnsutils docbook-xml docbook-xsl flex gdb libjansson-dev krb5-user \
libacl1-dev libaio-dev libarchive-dev libattr1-dev libblkid-dev libbsd-dev \
libcap-dev libcups2-dev libgnutls28-dev libgpgme-dev libjson-perl \
libldap2-dev libncurses5-dev libpam0g-dev libparse-yapp-perl \
libpopt-dev libreadline-dev nettle-dev perl perl-modules pkg-config \
python-all-dev python-crypto python-dbg python-dev python-dnspython \
python3-dnspython python-gpgme python3-gpgme python-markdown python3-markdown \
python3-dev xsltproc zlib1g-dev liblmdb-dev lmdb-utils ntp  isc-dhcp-server && echo OK

sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
service ssh reload
#Faço backup do arquivo da interface de rede
cp -pRf /etc/network/interfaces /etc/network/interfaces.bkp
#Escrevo um novo arquivo de interface definindo um ip estático
cat <<EOF > /etc/network/interfaces
# The loopback network interface
auto lo
iface lo inet loopback
#The primary network interface
auto ens192
  iface ens192 inet static
    address 192.168.0.10
    netmask 255.255.255.0
    gateway 192.168.0.254
    domain-name-servers 192.168.0.6 192.168.0.7
EOF
#Reinicio o serviço de rede
/etc/init.d/networking restart
cat <<EOF >> /etc/ntp.conf
#Implementing Samba 4
ntpsigndsocket /usr/local/samba/var/lib/ntp_signd/
restrict default mssntp
EOF
#Seto hostname
hostnamectl set-hostname ctic
systemctl restart systemd-hostnamed
#Faço meu host resolver pro meu ip
echo "192.168.0.10 ctic.admfibra.edu ctic" >> /etc/hosts
#Restarto o serviço de Rede
/etc/init.d/networking restart
#Muso o diretório para o /tmp
cd /tmp
#Faço download do samba em sua sersão 4.8.3
wget https://download.samba.org/pub/samba/stable/samba-4.8.0.tar.gz
#Descompacto os arquivos do samba
tar -zxvf samba-4.8.0.tar.gz
#Removo o Samba compactado
rm -f samba-4.8.0.tar.gz
#Entro na pasta do samba
cd samba-4.8.0
#Vamos gerar o Makefile e verificar se não há nenhuma dependência faltando e alguns ajustes extras
#./configure --enable-debug --enable-selftest
./configure --prefix /usr --enable-fhs --enable-cups --enable-debug --enable-selftest --sysconfdir=/etc --localstatedir=/var --with-privatedir=/var/lib/samba/private --with-piddir=/var/run/samba --with-automount --datadir=/usr/share --with-lockdir=/var/run/samba --with-statedir=/var/lib/samba --with-cachedir=/var/cache/samba --with-systemd
#Vamos compilar o Samba 4
make
#vamos instalar o samba
make install
#Configuração do kerberos
cat <<EOF > /etc/krb5.conf
[libdefaults]
	dns_lookup_realm = false
	dns_lookup_kdc = true
	default_realm = ADMFIBRA.EDU
EOF

cat <<EOF > /etc/samba/smb.conf
[global]
       security = ADS
       workgroup = ADMFIBRA
       realm = ADMFIBRA.EDU

       log file = /var/log/samba/%m.log
       log level = 1

       # Default ID mapping configuration for local BUILTIN accounts
       # and groups on a domain member. The default (*) domain:
       # - must not overlap with any domain ID mapping configuration!
       # - must use a read-write-enabled back end, such as tdb.
       # - Adding just this is not enough
       # - You must set a DOMAIN backend configuration, see below
       idmap config * : backend = tdb
       idmap config * : range = 3000-7999
       username map = /etc/samba/user.map
       enable privileges = yes
[dados]
      path = /dados/
      read only = no
EOF
echo "!root = ADMFIBRA\Administrador" > /etc/samba/user.map
vi /etc/nsswitch.conf
net ads join -U administrator
smbcontrol all reload-config
#net sam rigths grant "ADMFIBRA\Administrador" SeDiskOperatorPrivilege -I manga
net sam rights grant 'ADMFIBRA\TI_Servidor_CTIC' SeDiskOperatorPrivilege -U'ADMFIBRA\Administrador' -I manga

sed -i 's/passwd:       files/passwd:       files winbind/g' /etc/nsswitch.conf
sed -i 's/group:       files/group:       files winbind/g' /etc/nsswitch.conf

smbd
nmbd
winbindd
wbinfo --ping-dc

#Ferramenta que reconhece HDs em NTFS.
apt-get install ntfs-3g -y
#Verifique se o HD foi reconhecido.
fdisk -l
#Crio diretório de meu file server
mkdir /media/USBHD
#Definindo permissões a pasta criada
chmod 777 /media/USBHD
#Montando o HD externo na pasta
mount -t auto /dev/sdb1 /media/USBHD

[Backup]
comment = Pasta Arquivos
path = /media/USBHD/dados
valid users = @users
force group = users
create mask = 0660
directory mask = 0771
read only = no

echo "/dev/sda1 /media/USBHD auto noatime 0 0" >> /etc/fstab




cat <<EOF > /etc/init.d/samba-fs
#!/bin/sh
# chkconfig: 345 99 10
# description: smbd nmbd windbind

start() {
	echo "Iniciando o Samba"
	smbd >> /dev/null
	echo "Iniciando o Netbios"
	nmbd >> /dev/null
	echo "Iniciando o Winbindd"
	winbindd >> /dev/null
}

stop() {
	echo "Finalizando o Samba"
	pkill -9 smbd >> /dev/null
	echo "Finalizando o Netbios"
	pkill -9 nmbd >> /dev/null
	echo "Finalizando o Winbind"
	pkill -9 winbindd >> /dev/null
}

case "$1" in
start)
	start
	;;
stop)
	stop
	;;
status)
	status smbd
	status nmbd
	status winbindd
	;;
restart)
	stop
	start
	;;
*)
	echo $"Usage: $0 {start|stop|status|restart}"
	exit 2
esac
EOF

chmod a+x /etc/init.d/samba


#List services:
#ls /etc/init.d/

#Start service:
/etc/init.d/samba-fs start

#Stop service:
#/etc/init.d/samba-fs stop

#Enable service:
cd /etc/rc3.d
ln -s ../init.d/samba-fs S95samba-fs
#(the S95 is used to specify order. S01 will start before S02, etc)

#Disable service:
#rm /etc/rc3.d/samba-fs S95samba-fs